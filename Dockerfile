FROM php:7-cli as composer
WORKDIR /project
RUN apt-get update && apt-get install -y  unzip git libzip-dev zip 
RUN docker-php-ext-install zip
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/bin/composer
RUN mkdir /composer && chmod a+rxw /composer
ENV COMPOSER_HOME /composer

FROM composer as installer
WORKDIR /installer
RUN composer require laravel/installer
ENV PATH "$PATH:/installer/vendor/bin"
WORKDIR /project
RUN cd /tmp \
    && composer create-project --prefer-dist laravel/laravel cache \
    && cd cache \
    && composer require laravel/ui \
    && rm -rf /tmp/cache
RUN chmod -R 777 /composer
COPY installer-entrypoint.sh /installer-entrypoint.sh 
COPY ./example /example
ENTRYPOINT ["/installer-entrypoint.sh"]

FROM php:7-apache as web
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install ctype
RUN docker-php-ext-install fileinfo
RUN docker-php-ext-install json
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-install tokenizer
RUN apt-get update -y && apt-get install -y libxml2-dev
RUN docker-php-ext-install xml
RUN apt-get update -y && apt-get install -y libonig-dev
RUN docker-php-ext-install mbstring
RUN a2enmod rewrite
WORKDIR /project
COPY web/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY web/apache2.conf /etc/apache2/apache2.conf

FROM web as dev
RUN pecl install xdebug \
 && docker-php-ext-enable xdebug
COPY dev-entrypoint.sh /dev-entrypoint.sh
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY --from=installer /composer /composer
ENV COMPOSER_HOME /composer
ENTRYPOINT ["/dev-entrypoint.sh"]
CMD ["apache2-foreground"]
COPY php.ini /usr/local/etc/php/php.ini

FROM node as node
COPY node-entrypoint.sh /node-entrypoint.sh
WORKDIR /project
ENTRYPOINT ["/node-entrypoint.sh"]
