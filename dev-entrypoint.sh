#!/bin/sh
set -e 

if [ -n "$XDEBUG_DISABLE" ] 
then
	exec docker-php-entrypoint "$@"
fi

echo "XDebug disabled"
#rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
exec docker-php-entrypoint "$@"
